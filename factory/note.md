# 1. Mục đích
- để tạo ra các đối tượng sản phẩm mà không cần chỉ định các lớp cụ thể của chúng
- Factory Method định nghĩa một phương thức, nên được sử dụng để tạo các đói tượng thay vì gọi hàm trực tiếp (dùng `new`)
# 2. Cấu trúc.
![img.png](img.png)
- Products là 1 interface duy nhất cho tất cả các đối tượng có thể được tạo bởi Creator và các lớp con của nó.
- Concrete Products triển khai implementations Products Interface. Concrete Creators sẽ tạo và trả về các instances của các lớp này.
- Creator chính là factory method trả về Product type.
- Concrete Creators sẽ implement hoặc ghi đè factory method bằng cách tạo và trả lại một Concrete Products.
# 3. Ưu nhược
**Ưu:**
- tránh được những liên kết quá chặt chẽ giữa Creator và Concrete Products
- Tuân theo Single Responsibility Principle. 
- Tuân theo Open/Closed Principle. Bạn có thể thêm loại sản phẩm mới vào mà không phá vỡ client code.

**Nhược:**
- hệ thống source code trông sẽ phức tạp  