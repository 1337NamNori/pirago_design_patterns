<?php


namespace DesignPatterns\FactoryMethod;

class ShipFactory implements TransportFactory
{
    public function createTransport() : Transport
    {
        return new Ship();
    }
}