<?php

namespace DesignPatterns\FactoryMethod;

interface Transport
{
    public function deliver();
}