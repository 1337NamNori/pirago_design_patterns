<?php

namespace DesignPatterns\FactoryMethod;

class Ship implements Transport
{
    public function deliver()
    {
        echo "I deliver cargo by sea\n";
    }
}