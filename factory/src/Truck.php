<?php

namespace DesignPatterns\FactoryMethod;

class Truck implements Transport
{
    public function deliver()
    {
        echo "I deliver cargo by land\n";
    }
}