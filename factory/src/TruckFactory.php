<?php


namespace DesignPatterns\FactoryMethod;

class TruckFactory implements TransportFactory
{
    public function createTransport(): Transport
    {
        return new Truck();
    }
}