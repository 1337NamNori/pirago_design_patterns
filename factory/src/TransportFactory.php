<?php


namespace DesignPatterns\FactoryMethod;

interface TransportFactory
{
    public function createTransport() : Transport;
}