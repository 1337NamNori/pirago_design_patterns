<?php

require_once __DIR__ . "/vendor/autoload.php";

use DesignPatterns\FactoryMethod\ShipFactory;
use DesignPatterns\FactoryMethod\TruckFactory;

$shipFactory = new ShipFactory();
$ship = $shipFactory->createTransport();
$ship->deliver();

$truckFactory = new TruckFactory();
$truck = $truckFactory->createTransport();
$truck->deliver();