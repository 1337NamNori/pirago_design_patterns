<?php


namespace DesignPatterns\Facade;


class User
{
    public function authenticate()
    {
        echo 'Đã lấy thông tin người đặt hàng' . PHP_EOL;
    }
}