<?php


namespace DesignPatterns\Facade;


class Order
{
    public function store($userId, $productId, $quantity)
    {
        echo "Đã lưu thông tin đơn hàng" . PHP_EOL;
        echo "Người dùng: $userId" . PHP_EOL;
        echo "Sản phẩm: $productId" . PHP_EOL;
        echo "Số lương: $quantity" . PHP_EOL;
        echo "Thời gian đặt hàng: " . date('H:i:s d/m/Y') . PHP_EOL;
    }
}