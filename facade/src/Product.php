<?php


namespace DesignPatterns\Facade;


class Product
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function checkInStock($quantity)
    {
        echo "Còn đủ $quantity sản phẩm $this->id trong kho" . PHP_EOL;
    }

    public function decreaseInStock($quantity)
    {
        echo "Đã trừ $quantity sản phẩm $this->id trong kho" . PHP_EOL;
    }
}