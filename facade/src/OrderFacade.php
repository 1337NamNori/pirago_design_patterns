<?php


namespace DesignPatterns\Facade;


class OrderFacade
{
    private static $instances;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (!isset(self::$instances)) {
            self::$instances = new self;
        }
        return self::$instances;
    }

    public function order($userId, $productId, $quantity)
    {
        $product = new Product($productId);
        $user = new User();
        $order = new Order();

        $user->authenticate();
        $product->checkInStock($quantity);
        $order->store($userId, $productId, $quantity);
        $product->decreaseInStock($quantity);
    }
}