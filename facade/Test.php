<?php

require_once(__DIR__.'/vendor/autoload.php');

use DesignPatterns\Facade\OrderFacade;

$order = OrderFacade::getInstance()->order("1", "12345", 10);

