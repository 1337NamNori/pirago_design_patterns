# 1. Mục đích
- che giấu sự phức tạp của hệ thống con đằng sau
- thường dùng để xây dựng thư viện hoặc framework, giúp các lập trình viên dễ dàng làm ra website mà không cần hiểu sâu hệ thống
# 2. Cấu trúc
![img.png](img.png)
- **Facade:** Facade nắm rõ được hệ thống con nào đảm nhiệm việc đáp ứng yêu cầu của client, nó sẽ chuyển yêu cầu của client đến các đối tượng hệ thống con tương ứng.
- **Subsystems:** cài đặt các chức năng của hệ thống con, xử lý công việc được gọi bởi Facade. Các lớp này không cần biết Facade và không tham chiếu đến nó.
- **Client:** đối tượng sử dụng Facade để tương tác với các subsystem.
# 3. Ưu nhược
**Ưu:**
- có thể tách mã nguồn khỏi sự phức tạp của hệ thống con
- hệ thống tích hợp facade rất đơn giản, ta chỉ cần tương tác với facade.
- có thể đóng gói nhiều thiết kế không tốt thành một thiết kế tốt hơn.

**Nhược:**
- class Facade có thể trở nên quá lớn, làm quá nhiều nhiệm vụ và chức năng
- dễ dẫn đến vu phạm nguyên tắc SOLID.
