<?php


namespace DesignPatterns\Command;


class Control
{
    private $command;

    public function setCommand(Command $command)
    {
        $this->command = $command;
        return $this;
    }

    public function pressButton()
    {
        $this->command->execute();
    }
}