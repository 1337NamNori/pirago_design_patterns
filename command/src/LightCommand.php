<?php


namespace DesignPatterns\Command;


abstract class LightCommand implements Command
{
    protected $light;

    public function __construct(Light $light)
    {
        $this->light = $light;
    }
}