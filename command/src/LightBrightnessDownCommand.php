<?php


namespace DesignPatterns\Command;


class LightBrightnessDownCommand extends LightCommand
{
    public function execute()
    {
      $this->light->lightBrightnessDown();
    }
}