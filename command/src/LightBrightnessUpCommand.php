<?php


namespace DesignPatterns\Command;


class LightBrightnessUpCommand extends LightCommand
{
    public function execute()
    {
        $this->light->lightBrightnessUp();
    }
}