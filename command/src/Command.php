<?php

namespace DesignPatterns\Command;

interface Command
{
    public function execute();
}