<?php


namespace DesignPatterns\Command;


class Light
{
    public $on = false;
    public $brightnessLevel = 3;

    public function lightPower()
    {
        $this->on = !$this->on;
        echo 'Đèn đã được ' . ($this->on ? 'bật' : 'tắt').PHP_EOL;
    }

    public function lightBrightnessUp(){
        if ($this->brightnessLevel === 5) {
            echo "Đèn đã đạt độ sáng tối đa".PHP_EOL;
        } else {
            echo "Đèn đã tăng lên độ sáng " . ++$this->brightnessLevel.PHP_EOL;
        }
    }

    public function lightBrightnessDown(){
        if ($this->brightnessLevel === 0) {
            echo "Đèn đã đạt độ sáng tối thiểu".PHP_EOL;
        } else {
            echo "Đèn đã giảm xuống độ sáng " . --$this->brightnessLevel.PHP_EOL;
        }
    }
}