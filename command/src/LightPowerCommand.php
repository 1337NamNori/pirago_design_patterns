<?php


namespace DesignPatterns\Command;


class LightPowerCommand extends LightCommand
{
    public function execute()
    {
        $this->light->lightPower();
    }
}