# 1. Mục đích
- biến một request thành một object độc lập chứa tất cả thông tin về request;
- tham số hóa các đối tượng với request khác nhau như log, queue, undo, transaction,...
# 2. Cấu trúc
![img.png](img.png)
- **Command:** là một `interface` hoặc `abstract class`, chứa một phương thức trừu tượng thực thi (execute) một hành động (operation). Request sẽ được đóng gói dưới dạng Command.
- **ConcreteCommand:** là các implementation của Command. Định nghĩa một sự gắn kết giữa một đối tượng Receiver và một hành động. Thực thi execute() bằng việc gọi operation đang hoãn trên Receiver. Mỗi một ConcreteCommand sẽ phục vụ cho một case request riêng.
- **Client:** tiếp nhận request từ phía người dùng, đóng gói request thành ConcreteCommand thích hợp và thiết lập receiver của nó.
- **Invoker:** tiếp nhận ConcreteCommand từ Client và gọi execute() của ConcreteCommand để thực thi request.
- **Receiver:** đây là thành phần thực sự xử lý business logic cho case request. Trong phương thức execute() của ConcreteCommand chúng ta sẽ gọi method thích hợp trong Receiver.

![img_1.png](img_1.png)

Client và Invoker sẽ thực hiện việc tiếp nhận request. Còn việc thực thi request sẽ do Command, ConcreteCommand và Receiver đảm nhận.
# 3. Ưu nhược
**Ưu:**
- tuân theo Single Responsibility Principle. bạn có thể tách các lớp mà gọi các thao tác từ các lớp thực hiện thao tác này.
- tuân theo Open/Close Principle. Bạn có thể tạo các command mới vào app mà không cần phá vỡ client code hiện có.
- có thể triển khai undo/redo. 
- có thể triển khai việc hoãn các hoạt động.
- có thể tập hợp một nhóm các command phức tạp thành một command đơn giản.

**Nhược:**
- code có thể trở nên phức tạp vì bạn đang giới thiệu một lớp mới giữa Invoker và Receiver.