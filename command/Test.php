<?php

require_once(__DIR__ . '/vendor/autoload.php');

use DesignPatterns\Command\Control;
use DesignPatterns\Command\Light;
use DesignPatterns\Command\LightBrightnessDownCommand;
use DesignPatterns\Command\LightBrightnessUpCommand;
use DesignPatterns\Command\LightPowerCommand;

// test
$light = new Light();

$power = new LightPowerCommand($light);
$brightnessUp = new LightBrightnessUpCommand($light);
$brightnessDown = new LightBrightnessDownCommand($light);

class RemoteControl extends Control
{
}

class IntegratedControl extends Control
{
}

$remoteControl = new RemoteControl();
$integratedControl = new IntegratedControl();


$remoteControlPower = $remoteControl->setCommand($power);
$remoteControlPower->pressButton();

$integratedControlUp = $integratedControl->setCommand($brightnessUp);
$integratedControlUp->pressButton();
$integratedControlUp->pressButton();
$integratedControlUp->pressButton();

$remoteControlDown = $remoteControl->setCommand($brightnessDown);
$remoteControlDown->pressButton();
$remoteControlDown->pressButton();
$remoteControlDown->pressButton();
$remoteControlDown->pressButton();
$remoteControlDown->pressButton();
$remoteControlDown->pressButton();

$integratedControlUp->pressButton();

$integratedControlPower = $integratedControl->setCommand($power);
$integratedControlPower->pressButton();
