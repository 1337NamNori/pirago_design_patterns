<?php

require_once(__DIR__ . '/vendor/autoload.php');

use DesignPatterns\Strategy\DeliverByCar;
use DesignPatterns\Strategy\DeliverByMotor;
use DesignPatterns\Strategy\NoDeliver;
use DesignPatterns\Strategy\FreeShipping;
use DesignPatterns\Strategy\NoFreeShipping;
use DesignPatterns\Strategy\Product;

$refrigerator = new Product('Tủ lạnh Toshiba');
$refrigerator->setDeliveryBehavior(new DeliverByCar());
$refrigerator->setDeliveryFeeDiscountBehavior(new FreeShipping());
$refrigerator->log();

$laptop = new Product('Laptop HP');
$laptop->setDeliveryBehavior(new DeliverByMotor());
$laptop->setDeliveryFeeDiscountBehavior(new FreeShipping());
$laptop->log();

$cable = new Product('Dây sạc Anker');
$cable->setDeliveryBehavior(new DeliverByMotor());
$cable->setDeliveryFeeDiscountBehavior(new NoFreeShipping());
$cable->log();

$phoneCase = new Product('Ốp lưng iPhone 12');
$phoneCase->setDeliveryBehavior(new NoDeliver());
$phoneCase->setDeliveryFeeDiscountBehavior(new NoFreeShipping());
$phoneCase->log();