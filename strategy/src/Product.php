<?php


namespace DesignPatterns\Strategy;


class Product
{
    protected $name;
    public DeliveryBehavior $deliveryBehavior;
    public DeliveryFeeDiscountBehavior $deliveryFeeDiscountBehavior;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        echo $this->name . PHP_EOL;
    }

    public function setDeliveryBehavior(DeliveryBehavior $deliveryBehavior)
    {
        $this->deliveryBehavior = $deliveryBehavior;
    }

    public function setDeliveryFeeDiscountBehavior(DeliveryFeeDiscountBehavior $deliveryFeeDiscountBehavior)
    {
        $this->deliveryFeeDiscountBehavior = $deliveryFeeDiscountBehavior;
    }

    public function performDeliver()
    {
        $this->deliveryBehavior->deliver();
    }

    public function applyDiscount()
    {
        $this->deliveryFeeDiscountBehavior->discount();
    }

    public function log()
    {
        $this->getName();
        $this->performDeliver();
        $this->applyDiscount();
        echo "-----------------------" . PHP_EOL;
    }
}