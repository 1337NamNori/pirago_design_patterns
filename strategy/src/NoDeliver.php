<?php


namespace DesignPatterns\Strategy;


class NoDeliver implements DeliveryBehavior
{
    public function deliver()
    {
        echo "Sản phẩm không hỗ trợ giao hàng" . PHP_EOL;
    }
}