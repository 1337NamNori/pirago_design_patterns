<?php


namespace DesignPatterns\Strategy;


class DeliverByMotor implements DeliveryBehavior
{
    public function deliver()
    {
        echo "Sản phẩm được giao bằng xe máy" . PHP_EOL;
    }
}