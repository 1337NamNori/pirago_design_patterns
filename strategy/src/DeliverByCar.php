<?php


namespace DesignPatterns\Strategy;


class DeliverByCar implements DeliveryBehavior
{
    public function deliver()
    {
        echo "Sản phẩm được giao bằng ô tô" . PHP_EOL;
    }
}