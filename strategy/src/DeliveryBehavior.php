<?php

namespace DesignPatterns\Strategy;

interface DeliveryBehavior
{
    public function deliver();
}