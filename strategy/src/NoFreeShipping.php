<?php


namespace DesignPatterns\Strategy;


class NoFreeShipping implements DeliveryFeeDiscountBehavior
{
    public function discount()
    {
        echo "Sản phẩm này không được miễn phí ship" . PHP_EOL;
    }
}