<?php


namespace DesignPatterns\Strategy;


class FreeShipping implements DeliveryFeeDiscountBehavior
{
    public function discount()
    {
        echo "Sản phẩm này được miễn phí ship" . PHP_EOL;
    }
}