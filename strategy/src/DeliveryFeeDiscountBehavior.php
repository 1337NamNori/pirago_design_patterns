<?php


namespace DesignPatterns\Strategy;


interface DeliveryFeeDiscountBehavior
{
    public function discount();
}