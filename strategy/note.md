# 1. Mục đích
- định nghĩa những nhóm thuật toán phụ thuộc vào một loại hành vi.
- đặt mỗi thuật toán vào những class riêng biệt.
- làm cho các đối tượng có thể thay đổi cách thức thực hiện hành vi
# 2. Cấu trúc
 ![img.png](img.png)
- **Context:** tham chiếu đến một trong những **Concrete Strategy**, và chỉ giao tiếp với đối tượng này thông qua `interface` **Strategy**
- **Strategy:** `interface` **Strategy** là interface chung cho tất cả **Concrete Strategy**, nó khai báo mội phương thức mà **Context** sử dụng để thực thi một hành vi.
- **Concrete Strategy:** thực thi những cách thức thực hiện hành vi khác nhau.
- **Client:** tạo ra những object cụ thể, **Context** khai bảo những hàm setter để những object này có thể tự thay thế hành vi.
# 3. Ưu nhược
**Ưu:**
- Có thể đổi các thuật toán của hành vi ngay trong runtime.
- có thể tách các chi tiết triển khai của một thuật toán khỏi code sử dụng nó.  
- Có thể thay thế kế thừa với thành phần.
- Tuân theo nguyên lý Open/Close. Bạn có thể giới thiệu những strategy mới mà không cần thay đổi **Context**

**Nhược:**
- Nếu bạn chỉ có một vài thuật toán đơn giản và chúng ít khi thay đổi, thì việc áp dụng Strategy Pattern sẽ làm phức tạp chương trình.
- **Client** phải nhận thức sự khác nhau giữa các **Strategy** để chọn lựa một chiến lược phù hợp
- Một số ngôn ngữ lập trình hiện đại cho phép triên khai các phiên bản khác nhau của thuật toán bên trong một hàm ẩn danh, tương tự như cách dùng Strategy Pattern này nhưng giúp chương trình trông đơn giản hơn nhiều
# VD:
Cho các loại sản phẩm với các hành vi như sau:
- Tủ lạnh: free ship, ship bằng oto
- Laptop: free ship, ship bằng xe máy
- Dây sạc: không miễn phí ship, ship bằng xe máy.
- Ốp lưng: không miễn phí ship, không ship.