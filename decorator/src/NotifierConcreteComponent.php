<?php

namespace DesignPatterns\Decorator;

class NotifierConcreteComponent implements NotifierComponent
{
    public function send(\DateTime $date)
    {
        echo 'send email notification to user at ' . $date->format('H:i:s d/m/Y').PHP_EOL;
    }
}