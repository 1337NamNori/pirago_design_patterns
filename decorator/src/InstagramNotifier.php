<?php

namespace DesignPatterns\Decorator;

class InstagramNotifier extends NotifierDecorator
{
    public function sendInstagramNotification(\DateTime $date)
    {
        echo 'send Instagram notification to user at ' . $date->format('H:i:s d/m/Y').PHP_EOL;
    }

    public function send(\DateTime $date)
    {
        $this->notifierComponent->send($date);
        $this->sendInstagramNotification($date);
    }
}