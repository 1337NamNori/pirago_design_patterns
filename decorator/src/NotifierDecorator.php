<?php


namespace DesignPatterns\Decorator;


abstract class NotifierDecorator implements NotifierComponent
{
    protected $notifierComponent;

    public function __construct(NotifierComponent $notifierComponent)
    {
        $this->notifierComponent = $notifierComponent;
    }
}