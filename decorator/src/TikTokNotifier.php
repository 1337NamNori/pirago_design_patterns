<?php

namespace DesignPatterns\Decorator;

class TikTokNotifier extends NotifierDecorator
{
    public function sendTikTokNotification(\DateTime $date)
    {
        echo 'send TikTok notification to user at ' . $date->format('H:i:s d/m/Y').PHP_EOL;
    }

    public function send(\DateTime $date)
    {
        $this->notifierComponent->send($date);
        $this->sendTikTokNotification($date);
    }
}