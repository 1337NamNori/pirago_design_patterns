<?php


namespace DesignPatterns\Decorator;


interface NotifierComponent
{
    public function send(\DateTime $date);
}