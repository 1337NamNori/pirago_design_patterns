<?php

namespace DesignPatterns\Decorator;

class FacebookNotifier extends NotifierDecorator
{
    public function sendFacebookNotification(\DateTime $date)
    {
        echo 'send Facebook notification to user at ' . $date->format('H:i:s d/m/Y').PHP_EOL;
    }

    public function send(\DateTime $date)
    {
        $this->notifierComponent->send($date);
        $this->sendFacebookNotification($date);
    }
}