<?php

require_once (__DIR__.'/vendor/autoload.php');

use DesignPatterns\Decorator\NotifierComponent;
use DesignPatterns\Decorator\NotifierConcreteComponent;
use DesignPatterns\Decorator\FacebookNotifier;
use DesignPatterns\Decorator\InstagramNotifier;
use DesignPatterns\Decorator\TikTokNotifier;

// create new notifier
function getNotifier(): NotifierComponent
{
    return new NotifierConcreteComponent();
}

// add Facebook notify feature to a notifier
function addFacebookNotification(NotifierComponent $notifier): NotifierComponent
{
    return new FacebookNotifier($notifier);
}

// add Instagram notify feature to a notifier
function addInstagramNotification(NotifierComponent $notifier): NotifierComponent
{
    return new InstagramNotifier($notifier);
}

// add Instagram notify feature to a notifier
function addTikTokNotification(NotifierComponent $notifier): NotifierComponent
{
    return new TikTokNotifier($notifier);
}

// test
$notifier = getNotifier();
$notifier->send(new DateTime);

echo '-----------------------------'.PHP_EOL;

$facebookNotifier = addFacebookNotification($notifier);
$facebookNotifier->send(new DateTime);

echo '-----------------------------'.PHP_EOL;

$instagramNotifier = addInstagramNotification($notifier);
$instagramNotifier->send(new DateTime);

echo '-----------------------------'.PHP_EOL;

$instagramFacebookNotifier = addFacebookNotification($instagramNotifier);
$instagramFacebookTikTokNotifier = addTikTokNotification($instagramFacebookNotifier);
$instagramFacebookTikTokNotifier->send(new DateTime);

