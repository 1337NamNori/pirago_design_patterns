# 1. Mục đích
- cho phép chúng ta thêm các hành vi mới vào 1 đối tượng đã có
- thêm chức năng vào đối tượng đã có mà không ảnh hưởng đến các đối tượng khác.
# 2. Nguyên tắc
- khi cần thêm tính năng thì wrap đối tượng đã có trong một đối tượng mới 
- đối tượng mới gọi là đối tượng wrapper
- thông qua đối tượng wrapper này chúng ta sẽ cung cấp thêm các hành vi mong muốn.
# 3. Cấu trúc
![img.png](img.png)
- **Component:** là một interface quy định các method chung cần phải có cho tất cả các thành phần tham gia vào mẫu này.
- **ConcreteComponent:** là lớp implements các phương thức của Component.
- **Decorator:** là một abstract class dùng để duy trì một tham chiếu của đối tượng Component và đồng thời cài đặt các phương thức của Component  interface.
- **ConcreteDecorator:** là lớp kế thừa các phương thức của Decorator, nó cài đặt thêm các tính năng mới cho Component.
- **Client:** đối tượng sử dụng Component.
# 4. Ưu nhược
**Ưu:**
- có thể mở rộng hành vi của object mà khong cần tạo một class con mới.
- có thể thêm bớt trách nhiệm của một object trong thời gian chạy.
- có thể phối hợp nhiều hành vi bằng cách wrap một object vào nhiều decorator.
- tuân theo Single Responsibility Principle. 

**Nhược:**
- cấu hình code bạn đầu có thể trông khá xấu.