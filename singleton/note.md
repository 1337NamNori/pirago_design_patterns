# 1. Mục đích
Singleton đảm bảo rằng mỗi class chỉ có một instance object, đồng thời cho phép instance object này có thể được truy cập toàn cục
# 2. Khi nào nên dùng?
**VD:**
- connect Database
- ghi log
# 3. Cấu trúc
![img.png](img.png)
- khai báo phương thức `__construct()` là `private` (hoặc `protected` để kế thừa) để ngăn việc khởi tạo instance trực tiếp
- tạo một thuộc tính `private static` để chỉ instance.
- tạo một phương thức `public` để get instance. Nếu instance chưa được khởi tạo thì khởi tạo, rồi sau đó `return` instance.
# 4. Ưu nhược
**Ưu:**
- Dễ dàng cài đặt và triển khai
- Đảm bảo rằng chỉ có duy nhất một instance của một lớp nào đó được tạo ra trong suốt quá trình thực thi chương trình nhờ đó giúp tiết kiệm tài nguyên hệ thống.

**Nhược:**
Được xem như một anti-pattern và gây tranh cãi vì

- Vi phạm nguyên tắc đơn nhiệm (Single Responsibility Principle – SRP) vì vừa phải làm nhiệm vụ riêng, vừa phải đảm bảo số lượng instance, vừa phải cung cấp quyền truy cập toàn cục cho instance
- gây khó khăn trong quá trình kiểm thử
- có thể lỗi khi làm việc với multi-thread.
