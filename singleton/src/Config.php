<?php

namespace DesignPatterns\Singleton;

class Config
{
    private static $config;

    private function __construct()
    {
    }

    public static function getConfig()
    {
        if (self::$config === NULL) {
            self::$config = new Config();
        }
        return self::$config;

    }

    private $hashmap = [];

    public function getValue(string $key): string
    {
        return $this->hashmap[$key];
    }

    public function setValue(string $key, string $value): void
    {
        $this->hashmap[$key] = $value;
    }
}