<?php

require_once(__DIR__ . '/vendor/autoload.php');

use DesignPatterns\Singleton\Config;

$config1 = Config::getConfig();
$login = "test_login";
$password = "test_password";
$config1->setValue("login", $login);
$config1->setValue("password", $password);

echo "config1.login: ".$config1->getValue('login')."\n";
echo "config1.password: ".$config1->getValue('password')."\n";

$config2 = Config::getConfig();

echo "config2.login: ".$config2->getValue('login')."\n";
echo "config2.password: ".$config2->getValue('password')."\n";

if ($login == $config2->getValue("login") &&
    $password == $config2->getValue("password")
) {
    echo "Config singleton works fine.";
}